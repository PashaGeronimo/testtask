require('./env');
const express = require('express');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
require('./modules/schema');
app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
let port;
let argv = require('minimist')(process.argv.slice(2));
if ('port' in argv) {
    port = argv['port'];
} else {
    port = 8080;
}
app.set('port', port);
app.listen(port);
console.log('Express server started on localhost:' + port);

const cors = require('cors');
app.use(cors({
    exposedHeaders: ['authorization']
}));


app.use(fileUpload());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.get('/',(req,res)=>{
    res.send('Server works');
});

let user = require('./routes/user');
app.use('/user',user);

let book = require('./routes/book');
app.use('/book',book);


app.use((req, res) => {
    res.status(404).send({ error: 'Not Found' });
})
