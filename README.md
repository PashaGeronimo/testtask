# Api Backend 

## Setup

Install the dependencies by running 

```
npm install
```

### Configure connections MYSQL
change data in env.js

```
global.credDb = {
    User: 'root',
    Pass: '123456',
    Base:'basename'
}
```

### Fill the database with test data 

```
npm run filldb
```

### Start api server

```
npm run start
```


