const { User,Book, Author, Op, Enrolment, sequelize } = require('./modules/schema');



setTimeout(async function () {
    await sequelize.sync({force: true});

    await Book.create({name:'Сказки', photo:'2020-2-3_12_41_45.jpg'}).catch(err => {});
    await Book.create({name:'Детективы', photo:'2020-2-3_12_42_30.jpg'}).catch(err => {});

    await Book.create({name:'Фантастика', photo:'2020-2-3_12_43_6.jpg'}).catch(err => {});

    await Book.create({name:'Непобедимый', photo:'2020-2-3_12_44_22.jpg'}).catch(err => {});

    await Book.create({name:'Пикник на обочине', photo:'2020-2-3_12_45_26.jpg'}).catch(err => {});

    await Book.create({name:'Марсианин', photo:'2020-2-3_12_45_56.jpg'}).catch(err => {});



    await Author.create({firstName:'Александр', lastName:'Пушкин'}).catch(err => {});
    await Author.create({firstName:'Корней', lastName:'Чуковский'}).catch(err => {});

    await Author.create({firstName:'Агата', lastName:'Кристи'}).catch(err => {});
    await Author.create({firstName:'Борис', lastName:'Акунин'}).catch(err => {});
    await Author.create({firstName:'Артур', lastName:'Конан Дойл'}).catch(err => {});

    await Author.create({firstName:'Джон', lastName:'Толкин'}).catch(err => {});
    await Author.create({firstName:'Андрей', lastName:'Белянин'}).catch(err => {});

    await Author.create({firstName:'Станислав', lastName:'Лем'}).catch(err => {});

    await Author.create({firstName:'Аркадий', lastName:'Стругацкий'}).catch(err => {});
    await Author.create({firstName:'Борис', lastName:'Стругацкий'}).catch(err => {});

    await Author.create({firstName:'Энди', lastName:'Вейер'}).catch(err => {});

    await User.create({userName:'Пользователь1', email:'user1@mail.net', password:'$2b$10$r42A8tGTHL3WzMBBXjON/uuol5JCFaXB1XYiY8f/usDBbzmgvhMbi'}).catch(err => {});
    await User.create({userName:'Пользователь2', email:'user2@mail.net', password:'$2b$10$r42A8tGTHL3WzMBBXjON/uuol5JCFaXB1XYiY8f/usDBbzmgvhMbi'}).catch(err => {});

    await Book.update( {userId:1}, {where: {id: 2}}).catch(err => console.log(err));
    await Book.update( {userId:1}, {where: {id: 3}}).catch(err => console.log(err));

    await Author.findOne({where: {firstName: 'Александр'}})
        .then(author=>{
            if(!author) return;
            Book.findOne({where: {name: 'Сказки'}})
                .then(book=>{
                    if(!book) return;
                    author.addBook(book);
                });
        });

    await Author.findOne({where: {firstName: 'Корней'}})
        .then(author=>{
            if(!author) return;
            Book.findOne({where: {name: 'Сказки'}})
                .then(book=>{
                    if(!book) return;
                    author.addBook(book);
                });
        });



    await Author.findOne({where: {firstName: 'Агата'}})
        .then(author=>{
            if(!author) return;
            Book.findOne({where: {name: 'Детективы'}})
                .then(book=>{
                    if(!book) return;
                    author.addBook(book);
                });
        });

    await Author.findOne({where: {firstName:'Борис', lastName:'Акунин'}})
        .then(author=>{
            if(!author) return;
            Book.findOne({where: {name: 'Детективы'}})
                .then(book=>{
                    if(!book) return;
                    author.addBook(book);
                });
        });

    await Author.findOne({where: {firstName: 'Артур'}})
        .then(author=>{
            if(!author) return;
            Book.findOne({where: {name: 'Детективы'}})
                .then(book=>{
                    if(!book) return;
                    author.addBook(book);
                });
        });



    await Author.findOne({where: {firstName: 'Джон'}})
        .then(author=>{
            if(!author) return;
            Book.findOne({where: {name: 'Фантастика'}})
                .then(book=>{
                    if(!book) return;
                    author.addBook(book);
                });
        });
    await Author.findOne({where: {firstName: 'Андрей'}})
        .then(author=>{
            if(!author) return;
            Book.findOne({where: {name: 'Фантастика'}})
                .then(book=>{
                    if(!book) return;
                    author.addBook(book);
                });
        });



    await Author.findOne({where: {firstName: 'Станислав'}})
        .then(author=>{
            if(!author) return;
            Book.findOne({where: {name: 'Непобедимый'}})
                .then(book=>{
                    if(!book) return;
                    author.addBook(book);
                });
        });




    await Author.findOne({where: {firstName:'Аркадий', lastName:'Стругацкий'}})
        .then(author=>{
            if(!author) return;
            Book.findOne({where: {name: 'Пикник на обочине'}})
                .then(book=>{
                    if(!book) return;
                    author.addBook(book);
                });
        });


    await Author.findOne({where: {firstName:'Борис', lastName:'Стругацкий'}})
        .then(author=>{
            if(!author) return;
            Book.findOne({where: {name: 'Пикник на обочине'}})
                .then(book=>{
                    if(!book) return;
                    author.addBook(book);
                });
        });




    await Author.findOne({where: {firstName:'Энди'}})
        .then(author=>{
            if(!author) return;
            Book.findOne({where: {name: 'Марсианин'}})
                .then(book=>{
                    if(!book) return;
                    author.addBook(book);
                });
        });






}, 1000);

