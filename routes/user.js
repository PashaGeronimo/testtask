const express = require('express');
const router = express.Router();
const { User,Book, Author, Op, Enrolment } = require('../modules/schema');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
let jwtKey='sdftgbuishptgubsdjgwggyjugzignbdo';


router.get('/', async (req, res) => {

    res.json('user');
});

router.post('/register', async (req, res) => {
    let input = req.body;
    if (!input.password || !input.email  ) return res.status(400).send({error:"need to send email and password"});
    let checkUser = await User.findOne({ where: {email: input.email} }).catch(err => console.log(err));
    if (checkUser) {
        return res.status(400).send({error:"this email already exists"});
    }

    let hash = await bcrypt.hash(input.password, saltRounds);
    input.password = hash;


        let userRes = await User.create(input);


        res.send({result: 'User has been registered.'})


});

router.post('/login', async (req, res, next) => {
    let input = req.body;
    let email = input.email || '';
    let password = input.password || '';
    const user = await User.findOne({where: {email: email}}, (err) => { if (err) console.log(err)  });
    if (!user)  return res.status(400).send({error: 'wrong email or password'})

    let hash = user.password.replace(/^\$2y/, '$2b');
    if (!bcrypt.compareSync(password, hash))  return res.status(400).send({error: 'wrong email or password'});
    const token = createAccessToken(user.id, 'user');
    res.header('authorization', token).send({result: 'User logined.'})

})

router.get('/free', async (req, res) => {

    let resp = await Book.findAll({
        attributes: {exclude: ['createdAt', 'updatedAt', 'userId']},
                where: {
                    userId: {
                        [Op.or]: [null, '']
                    }
                },
        include: [
            {
                attributes: {exclude: ['createdAt', 'updatedAt']},
                model: Author, as: 'Authors',
                through: { attributes: [] }
            }
        ]

    }).catch(err => console.log(err));



    res.send({Books: resp });






});

router.get('/my', verifyUserToken, async (req, res) => {
    let userID = req.userDoc.id;
    let resp = await Book.findAll({
        attributes: {exclude: ['createdAt', 'updatedAt', 'userId']},
        where: {
            userId: userID
        },
        include: [
            {
                attributes: {exclude: ['createdAt', 'updatedAt']},
                model: Author, as: 'Authors',
                through: { attributes: [] }
            }
        ]

    }).catch(err => console.log(err));
    res.send({Books: resp });

});

router.post('/take', verifyUserToken,  async (req, res) => {
    let userID = req.userDoc.id;
    let selectedBooks = req.body.books;

    let resp = await Book.findAll({
        attributes: {exclude: ['createdAt', 'updatedAt', 'userId']},
                where: {
                    userId: userID
                }
    }).catch(err => console.log(err));

    if (resp.length + selectedBooks.length > 5) {
       return res.status(405).send({error: 'Maximum 5 books' });
    } else {
        for (book of selectedBooks) {
            let resp = await Book.update( {userId:userID}, {where: {id: book}}).catch(err => console.log(err));
        }

        res.send({status: 'Updated' });
    }



});

router.post('/return', verifyUserToken,  async (req, res) => {
    let userID = req.userDoc.id;
    let selectedBooks = req.body.books;
    for (book of selectedBooks) {
        let resp = await Book.update( {userId:null}, {where: {id: book}}).catch(err => console.log(err));
    }

    res.send({status: 'Updated' });

});



function createAccessToken(userId,role) {
    const token = jwt.sign({ id: userId,role:role}, jwtKey);
    return token;
}

async function verifyUserToken(req, res, next) {
    const token = req.headers["x-access-token"] || req.headers["authorization"];
    if (!token) return res.status(401).send({error: "no token provided"});

    try {
        const decodedPerson = jwt.verify(token, jwtKey);
        if (decodedPerson.role === 'user') {
            let user = await User.findByPk(decodedPerson.id);
            if (!user) {
                return res.status(404).send({error: 'this user has been deleted'})
            }
            req.userDoc = user;
            next();
        } else {
            return res.status(405).send({error: 'this token is not for user authorization'})
        }

    } catch (err) {
        if (err.name ==='TokenExpiredError') {return  res.status(400).send({error:'token expired'});}
        return res.status(400).send({error: "invalid token"});
    }
};

module.exports = router;
