const express = require('express');
const router = express.Router();
const { User,Book, Author, Op, Enrolment } = require('../modules/schema');
const path = require('path');
const fs = require('fs');
let  dir = path.join(__dirname , '../covers/');
const resizeImg = require('resize-img');

router.get('/', async (req, res) => {

    res.json('books');
});

router.get('/free', async (req, res) => {

    let resp = await Book.findAll({
        attributes: {exclude: ['createdAt', 'updatedAt', 'userId']},
/*        where: {
            userId: {
                [Op.or]: [null, '']
            }
        },*/
        include: [
            {
                attributes: {exclude: ['createdAt', 'updatedAt']},
                model: Author, as: 'Authors',
                through: { attributes: [] }
            }
        ]

    }).catch(err => console.log(err));



    res.send({Books: resp });






});


router.get('/authors', async (req, res) => {
    Author.findAll({
        attributes: {exclude: ['createdAt', 'updatedAt', 'id']},
    })
        .then(authors=>{
            res.send({authors: authors});
        });


});

router.post('/add', async (req, res) => {
    let book = req.body.book;
    let authors = req.body.authors;
    if (!book || !authors || authors.length === 0 )  return res.status(405).send({error: 'No input data'});
    let defaults = {
        name: book
    }
    if (req.body.photo) defaults.photo=req.body.photo
    let result =  await Book.findCreateFind({
            where: {
                name: book
            },
            defaults: defaults
        });
        if (!result[1]) return res.status(405).send({error: 'Book already exists'});

    for (author of authors) {
        await Author.findCreateFind({
                where: author,
                defaults: author
            }
        );
    }

    Book.findOne({where: {name: book}})
        .then(book => {

            for (author of authors) {
                Author.findOne({
                    where: author
                }).then(athr => {
                    book.addAuthor(athr);
                });
            }

        });

    res.send({req: req.body});
});

router.post('/photo', async (req, res) => {


        if (Object.keys(req.files).length == 0) {
            return res.status(400).send('No files were uploaded.');
        }
        if (req.files.file.mimetype !== 'image/jpeg' ) {
            return res.status(400).send('Not support type');
        }
        let sampleFile = req.files.file;
        let d = new Date();
        d = d.toLocaleDateString() + '_' +  d.getHours() + '_' + d.getMinutes() + '_' + d.getSeconds();
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
        }
        sampleFile.mv(dir +  d + '.jpg', function(err) {
            if (err)
                return res.status(500).send(err);
            (async () => {
                const image = await resizeImg(fs.readFileSync(dir +  d + '.jpg'), {
                    width: 250,
                });

                fs.writeFileSync(dir +  d + '.jpg', image);
            })();

            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify({ Data: d + '.jpg' }));
        });

});

router.get('/photo/:id', (req, res) => {
    try {
        res.sendFile(dir + req.params.id);
    }catch (e) {
        console.log(e);
    }
});


module.exports = router;
