require('../env');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(credDb.Base, credDb.User, credDb.Pass, Db,
    {timestamps: false}
);


sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });



const User = sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    userName: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING,
        unique: true
    },
    password: {
        type: Sequelize.STRING
    },
    role:{
        type:Sequelize.STRING
    }

});

const Book = sequelize.define('book', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING
    },
    photo: {
        type: Sequelize.STRING
    },
    date:{
        type:Sequelize.DATE
    }

});

const Author = sequelize.define('author', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    firstName: {
        type: Sequelize.STRING
    },
    lastName: {
        type: Sequelize.STRING,

    },
});

const Enrolment = sequelize.define('enrolment', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },

});


User.hasMany(Book, {as: 'book'});
//Book.hasMany(Author, {as: 'author'});


Book.belongsToMany(Author, {as: 'Authors', through: Enrolment});
Author.belongsToMany(Book, {as: 'Books', through: Enrolment});


sequelize.sync();

exports.User = User;
exports.Book = Book;
exports.Author = Author;
exports.Enrolment = Enrolment;
exports.Op = Sequelize.Op;
exports.sequelize = sequelize;
exports.Sequelize = Sequelize;
